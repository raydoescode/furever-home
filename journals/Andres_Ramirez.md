Day 14 - 7/20

Yesterday, our team completed the minimum viable product for our web application. Going forward, we will be waiting for the Riley's deployment lecture and will focus on studying for the assessment until that time. We will journal should we decide to return the project for any additional styling. No blockers.

Day 13 - 7/19

Yesterday, our team completed unit testing for all backend dog router functions. Today we will work on cleaning up our code (removing excessive console.log statements, making sure html input types are correct, etc.). No blockers

Day 12 - 7/18

Today, we will work on making sure all of our unit tests work and organizing them into a single file. We will also tackle making the edit form autopopulate with the given dogs existing information. No blockers.

Day 11 - 7/17

Last Friday, our team completed the minimum viable product for FurEverHome as a full-stack application. Today we will split up to try and create functioning unit tests. No blockers.

Day 10 - 7/14

Yesterday, our team completed the create and edit pages, as well as the list dogs page. Today we will wrap up the MVP frontend by including the MyDog page along with its associated edit and delete buttons. No blockers.

Day 9 - 7/13

Yesterday, our team completed the login and signup functions and began the create dog form and list dog page. Today we will be working on those pages, along with rearranging our database to include email so that we can display contact information on the frontend. No blockers.

Day 8 - 7/12

Yesterday, our team sorted through a number of bugs in the login and signup functions. Today we will be working through those bugs along with signout, and if there is any time, list dogs. No blockers.

Day 7 - 7/11

Yesterday, our team worked on the homepage, login, and signup functions. We struggled with various bugs relating to frontend auth. We plan on tackling those today.

Day 6 - 7/10

Before the break, our team completed all backend API endpoints for our MVP. Today, we worked on the frontend homepage, login, and signup forms. Our current blocker is frontend auth. We will be looking at that more closely following the lecture tomorrow.

Day 5 - 6/30

Yesterday, our team finished all backend API endpoints aside from update, which we started but had a bug on. Today, we will be debugging the update API endpoint, reviewing our codebase, and starting the frontend. No blockers.

Day 4 - 6/29

Yesterday, our team successfully established a foreign key relationship between the account and dog tables. We also finished the create dog API endpoint. Today, we plan on tackling list dog, delete dog, and edit dog. No blockers.

Day 3 - 6/28

Yesterday, our team finished backend authentication except for the duplicate error class. Today, we plan on tackling that as well as as many API endpoints as we can. No blockers.

Day 2 - 6/27

Last night I spent about 4 hours studying authentication in SQL and encountered some errors I have yet to resolve. Today our team will attack authentication as a unit. No blockers.

Day 1 - 6/26

We worked on setting up Trello to be used in lieu of Gitlab issue board. Tonight, we will finish the authentication video. No blockers.
