07/20/23
Yesterday, our team officially completed the MVP, performed code cleanup, and ran flake8 once more. We plan to study for the assessment until Riley's deployment lecture, otherwise if any styling is added we will do so under a stretch goals branch. No blockers.

07/19/23
Today, we will be focusing on fixing our last bug with the signup/login page where it is flashing username already exists. We will also write a few try/except blocks for backend error handling and fix our html input types for our forms on the frontend. Otherwise, all that is left is general code cleanup!

07/18/23
Today, we will be adding our functional unit tests to the unit test branch. We will also be doing some housekeeping with some small issues we've seen on our frontend, such as our edit form not autopopulating. If there is time, we will go through our final checklist. No blockers.

07/17/23
During lecture this morning, we fixed our flake8 errors and frontend errors. Today, we will be focusing on each writing our unit tests. No blockers.

07/14/23
Yesterday, we finished our list dog page and create dog form. Today, we will be focusing on our mydogs page and depending on time, maybe begin testing.

07/13/23
Today, our team will return to mob programming for the backend so that we can rearrange our database. Afterwards, we will continue working on our create dog form and list dog page as pairs. No blockers.

07/12/23
Today we successfully debugged front end auth and broke into pair programming for the create dog form and list dog page. Ray, Dre, and myself worked on the list dog page and managed to get our cards showing up successfully. We realized we will need to go back to the backend to add an email field.

07/11/23
Starting with auth as a blocker. Debugging auth will be our focus today before moving onto any other parts.

07/10/23
We officially completed the backend before going on summer break. Today is our first day back and we got off to a good start but have encountered some errors on our front end auth. We went through the exploration and decided we should watch the lecture tomorrow before digging into it more. Will continue working on other parts of our front end.

06/30/23
We accomplished a lot of work yesterday on the backend, but ended on debugging our update query. We are almost done with the backend and hoping to be finished today. We will be picking up with where left off yesterday.

06/29/23
This morning we successfully fixed the errors from yesterday. Starting the day with no blockers. We are continuing work on the backend today and made a goal to finish it tomorrow.

06/28/23
Today we discussed some backend goals for the week and decided we will focus solely on meeting requirements before adding on any personalization or stretch goals. We continued working on the backend, focusing on our dogs pydantic model and getting data into the database. We ran into some issues with the foreign key and were able to solve a few but still have some errors as blockers. Happy Birthday Dre!

06/27/23
Today we completed auth, tomorrow we will decide if we want to personalize right away or come back if there is time, then continue work on the backend. No blockers.

06/26/23
Today we received our repo and discussed the remaining items that we needed to collectively decide upon. After adding those as well as our tasks into our Trello, set up our repo. Then, we decided to focus on the JWTDown exploration to be ready to go for auth first thing tomorrow. No blockers.
